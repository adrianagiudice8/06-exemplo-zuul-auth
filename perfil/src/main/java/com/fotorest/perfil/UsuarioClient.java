package com.fotorest.perfil;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name="servidorautenticacao")
public interface UsuarioClient {
	
	@PostMapping("/usuario")
	public Perfil inserir(@RequestBody Perfil perfil);
}
